vim.g.mapleader = ","
vim.opt.compatible = false

require("packer").startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  use 'rhysd/vim-clang-format'
  use 'dracula/vim'
  use 'sainnhe/sonokai'
  use 'scrooloose/nerdcommenter'
  use 'vim-airline/vim-airline'
  use 'Vimjas/vim-python-pep8-indent'
  use {'google/yapf', {rtp = 'plugins/vim'}}
  use 'udalov/kotlin-vim'
  use 'cohama/lexima.vim'
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'saadparwaiz1/cmp_luasnip'
  use 'L3MON4D3/LuaSnip'
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
end)

vim.cmd("filetype plugin indent on")
vim.cmd("syntax enable")

vim.cmd("autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>")
vim.cmd("autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>")

-- Add spaces after comment delimiters by default
vim.g.NERDSpaceDelims = 1
vim.g.NERDCustomDelimiters = {python = {left = '#'}}
-- Align line-wise comment delimiters flush left instead of following code indentation
vim.g.NERDDefaultAlign = 'left'
-- Allow commenting and inverting empty lines (useful when commenting a region)
vim.g.NERDCommentEmptyLines = 1

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.cc = "81"
vim.cmd("highlight ColorColumn ctermbg=235")
vim.opt.number = true

vim.opt.cindent = true
-- Set c++ indentation of public, private keywords as
-- class A {
--  public:
--   void Stuff();
--  private:
--   int dummy_;
-- }
vim.opt.cinoptions:append("g1,h1")
-- Remove extra indentation inside namespace declaration.
vim.opt.cinoptions:append("N-s")

-- Trim trailing whitespace on save.
vim.cmd("autocmd FileType c,cpp,java,python autocmd BufWritePre <buffer> %s/\\s\\+$//e")

vim.g.airline_powerline_fonts = 1
vim.g["airline#extensions#tabline#enabled"] = 1
vim.g["airline#extensions#tabline#left_sep"] = ' '
vim.g["airline#extensions#tabline#left_alt_sep"] = '|'

local capabilities = require("cmp_nvim_lsp").default_capabilities()
local luasnip = require 'luasnip'

local nvim_lsp = require('lspconfig')

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)
vim.keymap.set('n', '=', '<c-a>')  -- increment number under cursor
vim.keymap.set('n', '-', '<c-x>')  -- decrement number under cursor

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end

nvim_lsp['clangd'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}
nvim_lsp['rust_analyzer'].setup{
  on_attach = on_attach,
  capabilities = capabilities,
}

-- nvim-cmp setup
cmp = require'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },

  sources = {
    { name = 'nvim_lsp' },
    { name = 'path' },
    { name = 'luasnip' },
  },

  mapping = cmp.mapping.preset.insert({
    ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
    ['<C-d>'] = cmp.mapping.scroll_docs(4), -- Down
    -- C-b (back) C-f (forward) for snippet placeholder navigation.
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
}

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

-- Configure the colour theme.
vim.g.sonokai_style= "atlantis"
vim.g.sonokai_disable_italic_comment = 1
vim.opt.termguicolors = true -- if you want to run vim in a terminal
vim.cmd("colorscheme sonokai")

-- Make scrollwheel not move the cursor but the code instead.
vim.opt.mouse = "a"

-- Turn all trailing whitespace into dots.
vim.opt.list = true
vim.opt.listchars = "tab:» ,trail:·"

-- Disable Ctrl-Z, remap to undo
vim.keymap.set("", "<C-z>", "u")
vim.keymap.set("i", "<C-z>", "<C-o>u")

vim.keymap.set({"n", "v"}, "d", '"_d')
vim.keymap.set("v", "p", '"_dP')

-- Tab stuff
-- map <C-t><up> :<CR>
-- map <C-t><down> :tabl<CR>
vim.keymap.set("", "<C-t><left>", ":bp<CR>")
vim.keymap.set("", "<C-t><right>", ":bn<CR>")

-- Yapf config
vim.keymap.set("", "<C-y>", ":YAPF<CR>")
vim.keymap.set("i", "<C-y>", "<c-o>:YAPF<CR>")
